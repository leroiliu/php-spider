<?php

namespace MicroCyanSpider\Plan\XiaoHongShu;

use MicroCyanSpider\Core\Kernel\BaseCore;
use MicroCyanSpider\Plan\XiaoHongShu\Helper\Http;

class App extends BaseCore
{
    /**
     * @var Http
     */
    protected $http;
    function __construct($config)
    {
        parent::__construct($config);
        $this->http = new Http();
    }

    /**
     * @description 获取小红书内容
     * @param $noteId
     * @param bool $checkCookie
     * @return array|mixed
     * @throws \Exception
     */
    public function getNote($noteId, bool $checkCookie=true){
        $pageContent = $this->http->request('get',"https://www.xiaohongshu.com/explore/{$noteId}");
        $pageContent = explode('window.__INITIAL_STATE__=',$pageContent);
        $pageContent = explode('</script>',$pageContent[1]??'');
        $pageContent = str_replace('\u002F','/',$pageContent[0]);
        $pageContent = str_replace('undefined','""',$pageContent);
        $pageContent = json_decode($pageContent,true);
        if ($checkCookie&&empty($pageContent['user']['userInfo']['userId'])) throw new \Exception('登录状态无效');
        return $pageContent['note']['noteDetailMap'][$noteId]['note']??[];
    }
    /**
     * @description 获取小红书作者信息
     * @param $userId
     * @param bool $checkCookie
     * @return array|mixed
     * @throws \Exception
     */
    public function getUserProfile($userId, bool $checkCookie=true){
        $pageContent = $this->http->request('get',"https://www.xiaohongshu.com/user/profile/{$userId}");
        $pageContent = explode('window.__INITIAL_STATE__=',$pageContent);
        $pageContent = explode('</script>',$pageContent[1]??'');
        $pageContent = str_replace('\u002F','/',$pageContent[0]);
        $pageContent = str_replace('undefined','""',$pageContent);
        $pageContent = json_decode($pageContent,true);
        if ($checkCookie&&empty($pageContent['user']['userInfo']['userId'])) throw new \Exception('登录状态无效');
        return $pageContent['user']['userPageData']??[];
    }
    /**
     * @description 获取小红书评论列表
     * @param $noteId
     * @param string $cursor
     * @param string $top_comment_id
     * @return array|mixed
     * @throws \Exception
     */
    public function getComments($noteId, string $cursor='', string $top_comment_id=''){
        $query = [];
        $query['image_scenes'] = 'FD_WM_WEBP,CRD_WM_WEBP';
        $query['cursor'] = $cursor;
        $query['note_id'] = $noteId;
        $query['top_comment_id'] = $top_comment_id;
        $pageContent = $this->http->request('get',"https://edith.xiaohongshu.com/api/sns/web/v2/comment/page",['query'=>$query]);
        return json_decode($pageContent??'{}',true)??[];
    }
    /**
     * @param string $cookie
     * @return $this
     */
    public function setCookie(string $cookie): App
    {
        $this->http->setCookie($cookie);
        return $this;
    }
    /**
     * @param string $proxy
     * @return $this
     */
    public function setProxy(string $proxy): App
    {
        $this->http->setProxy($proxy);
        return $this;
    }
    /**
     * @param array $headers
     * @return $this
     */
    public function setHeader(array $headers=[]): App
    {
        $this->http->setHeader($headers);
        return $this;
    }

}