<?php

namespace MicroCyanSpider\Core;

/**
 * @method static \MicroCyanSpider\Plan\XiaoHongShu\App XiaoHongShu(array $config)
 */
class Spider
{
    /**
     * @param $name
     * @param $arguments
     * @return \MicroCyanSpider\Plan\XiaoHongShu\App
     * @throws \Exception
     */
    public static function __callStatic($name, $arguments)
    {
        // TODO: Implement __callStatic() method.
//        error_reporting(E_ALL^E_NOTICE^E_WARNING^E_DEPRECATED);
        $name = strtolower($name);
        $name = str_replace('_','',$name);
        if ($name==='xiaohongshu'){
            return new \MicroCyanSpider\Plan\XiaoHongShu\App(...$arguments);
        }
        throw new \Exception('抱歉，暂无该方法');
    }



}