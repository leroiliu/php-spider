<?php

namespace MicroCyanSpider\Core\Kernel;

class BaseCore
{

    /**
     * @var mixed
     */
    protected $config = [];

    /**
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

}