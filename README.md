# 举例

> 首先登录小红书，再进行操作

## Cookie
![Alt 获取Cookie](https://gitee.com/leroiliu/php-spider/raw/master/img.png "获取Cookie")

## 代码
```php
    $Cookie = '';
    //参数举例
    $noteId = "6518d251000000002101c53f";
    $checkCookie = true;
    $userId = "64be068b000000002b008e72";
    $cursor="";
    $top_comment_id="";
    //初始化
    $xhs = \MicroCyanSpider\Core\Spider::XiaoHongShu([]);
    //设置Cookie---必须
    $xhs->setCookie($Cookie);
    //设置代理--非必填
    $xhs->setProxy('user:password@ip:port');
    //获取笔记数据
    $xhs->getNote($noteId,$checkCookie);
    //获取作者信息
    $xhs->getUserProfile($userId,$checkCookie);
    //获取笔记评论信息
    $xhs->getComments($noteId,$cursor,$top_comment_id);
```